# Processo Seletivo WinSocial - Arquiteto(a) de Sistemas

## Recrutando candidatos

A empresa LeadScore tem uma equipe de agentes de venda e recebe milhares de leads por dia. A empresa precisa entender qual deles é o mais indicado para recebê-los.

Seu papel aqui é ajudar o gerente de vendas indicando qual o vendedor com maior score. Calculamos o score de um vendedor cruzando o número de horas úteis (seg-sex 9am-6pm) sem receber um lead com a sua senioridade.

**Considerações gerais:**


Cáculo do score:

`SCORE = NH * MT` onde:

* **NH** é o número de horas úteis desde o último recebimento de um lead
* **MT** é o multiplicador associado à senioridade do vendedor

Níveis de senioridade:

* **1**: júnior
* **2**: pleno
* **3**: sênior

Para definir o número máximo de leads recebidos usamos a tabela abaixo:

|Nível do Vendedor|Multiplicador|
|-|-|
|Júnior|1|
|Pleno|1.5|
|Sênior|2|

Exemplo: 

O `SCORE` de um vendedor `Pleno` que não recebe um lead há `3 horas` será de `4.5`. Por outro lado, um sênior que não recebe um lead há `2.5 horas` terá um `SCORE` de `5`. Ou seja, o vendedor sênior receberia o lead por ter um `SCORE` mais alto.

### Especificações da API

#### Endpoints

##### 1. Criar um endpoint para cadastrar agentes com seus nomes e níveis de senioridade

Todo o desenho do endpoint (uri/verbo/request/response) para esta função será definido por você e **faz parte da avaliação**.

##### 2. Criar um endpoint para cadastrar leads com nome e telefone

Todo o desenho do endpoint para esta função será definido por você e **faz parte da avaliação**.

##### 3. Criar um endpoint para retornar os agentes mais qualificados para um lead específico (ordenados pelo score de forma decrescente).

Todo o desenho do endpoint para esta função será definido por você e **faz parte da avaliação**.

**Response:**

```json
[
    {
        "nome": "Vendedor Mary Jane",
        "nivel": 3,
        "ultimo_lead_recebido": "2020-11-19 09:22:11",
        "score": 36
	},
    {
        "nome": "Vendedor John Doe",
        "nivel": 1,
        "ultimo_lead_recebido": "2020-11-20 11:23:22",
        "score": 18	
    },
    ...
]
```